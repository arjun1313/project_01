package main

import (
	"database/sql"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

var db *sql.DB
var err error

var (
	sensor_id     string
	current_value string
)

func main() {
	db, err = sql.Open("mysql", "root:MyNewPass4!@/my_db")
	if err != nil {
		log.Fatal(err)
	}
	router := mux.NewRouter()
	router.HandleFunc("/Sensors", allsensor).Methods("GET")
	router.HandleFunc("/Sensors/{id}/", sensorbyid).Methods("GET")
	http.ListenAndServe(":8000", router)
}

func allsensor(w http.ResponseWriter, r *http.Request) {
	rows, err := db.Query("select sensor_id,current_value from Sensors")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&sensor_id, &current_value)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(sensor_id, current_value)
	}
}

func sensorbyid(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	//w.WriteHeader(http.StatusOK)
	//fmt.Fprintf(w, "id: %v\n", params["id"])
	//log.Printf("id: %v\n", params["id"])
	rows, err := db.Query("select current_value from Sensors where sensor_id = ?", params["id"])
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	rows.Scan(&current_value)
	log.Println(params["id"], current_value)
}
